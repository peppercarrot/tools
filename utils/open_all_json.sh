#!/bin/bash

cd /home/deevad/peppercarrot/webcomics/
for episode in ep*; do kate "$episode"/info.json; done
cd /home/deevad/peppercarrot/webcomics/miniFantasyTheater/
for id in .[0-9][0-9][0-9].json; do kate $id; done
cd /home/deevad/peppercarrot/webcomics/misc/
for json in [0-9]*; do kate "$json"/info.json; done
 
