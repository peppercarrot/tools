# Quick start

Just type:

    make

This will automatically generate all the files you need.

(See the last section for information on the software you
need to have installed for this to work).

If you have a CPU with multiple cores (you probably do),
use the `-j` argument to speed things up a bit,
e.g., `make -j4`.



# Details

It’s easiest to write the patron names
separated by newlines into the files `level-1.txt`,
`level-2.txt`, `level-3.txt` and `level-4.txt`
(where the names in `level-1.txt` will be
rendered with the largest font size,
the ones in `level-2.txt` with the second
largest etc.).

But if you prefer, you can put *all* the names in
the same file instead, `patronlistname.txt`,
with the following headings:

* [TOP]
* [5★★★★★]
* [3★★★]
* [ 1★]

All the names for a given patron level should be put
*on the same line*, separated by ‘ ★ ’ (without
the quotes).

Whenever you make changes to one of the files
(e.g., `level-2.txt`), just type `make` again
to automatically regenerate all files (recursively)
that depend on that file.

To delete all automatically generated files,
type `make clean`. Note that this does *not*
delete the `level-*.txt` files, since you
might have been using these (instead of
`patronlistname.txt`) as the source files.

To change the font sizes, line spacing (leading),
text width, colours or any other renderings details,
edit `patrons.tex` and run `make` again.

If you want to test changes without generating
*all* files, you can specify the name of the file(s)
you want to generate, e.g., `make patrons.pdf`.
(All intermediate files needed to build the file
will be generated, but not any files which just
depend on the file.)


# Dependencies

For the above to work, you’ll need to have these
fonts and these pieces of software installed:

## Fonts

* Lavi (https://framagit.org/peppercarrot/fonts)
* Droid Sans Fallback

## Software you probably already have

* sed
* (g)awk
* coreutils
* make

## Software you might not have

* LaTeX (e.g., ‘texlive’)
* GhostScript
* ImageMagick

LaTeX (LuaLaTeX) is used to typeset the names to a PDF file.
The system uses just a very few LaTeX packages (see the
`\usepackage` commands in `patrons.tex`), so you can get away
with only installing a minimal set of packages, but if you’re
not short on disk space, it’s perhaps easiest to just install
the entire TeX Live distro/meta-package.

``sudo apt install texlive-full``
4GB on 'buntu

GhostScript (the `gs` binary) is used to convert the PDF files to
PNG files, and ImageMagick (the `mogrify` binary) is used to crop
the PNG files.

# Known issue / Troubleshot:

- Single quote " character in name can bug the script. Use ”quote like that“ instead.

# Questions?

Feel free to contact me, Karl Ove Hufthammer, at karl@huftis.org.
