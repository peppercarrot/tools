NAMES_TXT=level-1.txt level-2.txt level-3.txt level-4.txt
NAMES_TEX=$(NAMES_TXT:.txt=.tex)
NAMES_PNG=$(NAMES_TXT:.txt=.png)
TMPFILES=patrons.pdf patrons.log patrons.aux $(NAMES_TEX) $(NAMES_PNG)

all: $(NAMES_PNG)

# Generate newline-separated .txt files with list of names
$(NAMES_TXT): patronlistname.txt
	@echo "Generating name .txt file: $@"
	@heading=`echo $@ | sed -e s/level-1.txt/TOP/ -e s/level-2.txt/5★★★★★/ -e s/level-3.txt/3★★★/ -e s/level-4.txt/\ 1★/`;\
	cat $< | sed -n '/^\['"$$heading"'\]$$/,/^$$/p' | tail -n+2 | sed 's/ *★ */\n/g' > $@

# Generate .tex files of all the name .txt files:
#   Trim leading and trailing whitespace
#   Wrap non-latin text in {\cjk }
#   Escape any special characters (except \, { and })
#   Replace spaces inside names by non-breaking spaces
#   Remove any trailing newlines
#   Put all names in \name{} commands
#   Remove the name separator from the last line
%.tex: %.txt
	@echo "Generating name .tex file: $@"
	@nlchars='[^][ !"\#$$%&'"'"'()*+,./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ^_`abcdefghijklmnopqrstuvwxyz{|}~¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿĆćĈĉėęĜĝĴĵŁłŒœŚśŜŝŠšŸŽžƒˆ˜–—‘’‚“”„†‡•…‰‹›€™ĀāĂăĄąĊČċčŭŬŰűŐőĒēĔĕĹĻĚěĎďĐđĖĘĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĶķĸĺļĽľĿŀŃńŅņŇňŉŊŋŌōŎŏŔŕŖŗŘřŞşŢţŤťŦŧŨũŪūŮůŲųŴŵŶŷŹźŻż˛˚ˇЁАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюяёєѕіїјѓҐґЄЅІЇЈЃњќЌЏЎўџЊЋћЉђљЂѲѣѢ-]'; \
	cat $< |\
	awk '{$$1=$$1;print}' |\
	sed 's/\('"\($$nlchars \?\)\+"'\)/{\\cjk \1}/g' | sed 's/ }/} /g' |\
	sed -e 's/\([&%$$#_]\)/\\\1/g' \
	    -e 's/~/\\textasciitilde/g' \
	    -e 's/\^/\\textasciicircum/g' |\
	sed -e 's/ /~/g' -e 's/\(\\[a-z]\+\)~/\1 /' |\
	awk NF |\
	awk '{print "\\name{" $$0 "} \\namesep"}' |\
	sed '$$s/ \\namesep//' > $@

# Typeset the patrons list
patrons.pdf: patrons.tex $(NAMES_TEX)
	@echo "Typesetting names to PDF: $@"
	@lualatex --halt-on-error --interaction=nonstopmode patrons.tex > output.txt

# Convert the PDF to separate PNG files
%.png: patrons.pdf
	@echo "Generating PNG image: $@"
	@pagenum=`echo $@ | sed 's/[^0-9]//g'` ; \
	  gs -q -dSAFER -dBATCH -dNOPAUSE -sDEVICE=pngalpha \
	     -dTextAlphaBits=4 -dGraphicsAlphaBits=4 \
	     -dFirstPage=$$pagenum -dLastPage=$$pagenum \
	     -r300 -sOutputFile=$@ $<
	@mogrify -quiet -trim +repage \
	         -bordercolor transparent -border 2 $@
	@convert $@ -resize 2466x\> $@

# Remove temporary/generated files
# (though not the level-*.txt files)
.PHONY clean:
	@echo -e "Removing temporary files"
	rm -f $(TMPFILES)
