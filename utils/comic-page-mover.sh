#!/bin/bash
# 

notice="Usage: Copy this script inside the episode folder; open a terminal in the episode directory, then run ./comic-page-mover.sh <string(eg. P05)> <replacement(eg. P09)>"
if [ ! $# -eq 2 ]; then
        echo ${notice}
        exit 
fi
IFS='
'

# Header
projectname="${PWD##*/}"
workingpath="${PWD}"
backupdrive="/media/data/backup"

Off=$'\e[0m'
Purple=$'\e[1;35m'
Blue=$'\e[1;34m'
Green=$'\e[1;32m'
Red=$'\e[1;31m'
Yellow=$'\e[1;33m'
White=$'\e[1;37m'
BlueBG=$'\e[1;44m'
RedBG=$'\e[1;41m'
PurpleBG=$'\e[1;45m'
Black=$'\e[1;30m'


isodate=$(date +%Y-%m-%d)
version=$(date +%Y-%m-%d_%Hh%M)
export input=$1
export output=$2
export error=0
export dryrun=0

clear
echo ""
echo "   ${White}${BlueBG}                  [ Comic page mover ]              ${Off}"
echo "   ${White}${BlueBG}                     version 0                  ${Off}"
echo "   ${White}${BlueBG}                     by David Revoy                 ${Off}"
echo ""

echo " Input/Search: $1 "
echo " Replacement: $2 "
echo ""
echo " projectname: $projectname "
echo " workingpath: $workingpath "
echo ""

cd $workingpath

# Main function
_function_rename_loop () {
  for search in $(find . -name "*$input*"); do
    path=${search%/*}
    filename=${search##*/}
    extension=$(echo $filename|sed 's/.*\(\.[a-zA-Z0-9]*\)$/\1/')
    # Check if the path still return a file.
    if [ -f ${search} ]; then
      replace=`echo ${search} | sed "s/${input}/${output}/"`

      echo "______________________________________________________________________________________________________"
      echo "I: ${search}"
      
      # First, check if the target file already exists. We don't overwrite.
      if [ -f ${replace} ]; then
        echo ${Red}"O: ${replace}"${Off}
        error=1
      else
        # If the script runs in Drymode; then it is testing/displaying only (useful for a first pass).
        if [ $1 == "dryrun" ]; then
          echo "${Green}O: ${replace}"${Off}
          # Else; we do the real move job (dangerous)
        else
          # Do the real move job
          mv "${search}" "${replace}"
          # Check if the file has moved to return feedback to user
          if [ -f ${replace} ]; then
             echo "${Green}O: ${replace} => Moved with success!"${Off}
              # SVG jobs
              if [ ${extension} == ".svg" ]; then
                echo "${Yellow}=> SVG FILE ${Off}"
                sed -i "s/${input}/${output}/g" ${replace}
              fi
          else
            echo "${Red}O: ${replace} => Can't move the file."${Off}
          fi
        fi
       
      fi



    fi
  done
}

_function_rename_loop "dryrun"

echo "======================================================================================================"
if [ ${error} == 1 ]; then
  echo " ${Red}ERROR ${Off}"
  echo " ${Red}=> Colision problem${Off}"
  echo "    Some files (in red) already exists in the destination."
  echo "    This script doesn't overwrite existing files."
  echo "    Fix the situation, and relaunch later."
  exit
else
  echo " ${Green}FILE CHECK IS OK ${Off}"
  echo "   No collision detected."

  echo "${Purple}  Do you want to apply this changes? (y/n)${Off}"
  read -p "?" ANSWER
  if [ "$ANSWER" = "y" ]; then
    _function_rename_loop "realrun"
  else
    exit
  fi
fi


# end notification
# notify-send " done : $output "
