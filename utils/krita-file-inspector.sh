#!/bin/bash

#: Title       : Pepper&Carrot krita-file-inspector.sh
#: Author      : David REVOY < info@davidrevoy.com >
#: License     : GPL

# This utility is a Krita file (*.kra) inspector.
# It find them, unzip them and check with grep if
# something problematic is found.

# Header
export Off=$'\e[0m'
export Green=$'\e[1;32m'
export Red=$'\e[1;31m'

# Generate temporary directory
mkdir /tmp/kramaindoccheck

# Loop
find ${HOME}/* -type f -name "*.kra" -print0 | while read -d $'\0' file ; do
    cd /tmp/kramaindoccheck
    unzip -qq ${file} maindoc.xml
    if grep -q '<IMAGE name="\/home' "maindoc.xml"; then
        echo "${Green}$file${Off}"
    fi
    rm /tmp/kramaindoccheck/maindoc.xml
done
