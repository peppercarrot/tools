#!/usr/bin/env python
# compatible with Python >=3.11, no dependencies except python itself

import sys
import json
from typing import TypedDict
from pathlib import Path


class PageMetadata(TypedDict):
    """Page metadata typing"""

    page_type: str
    filename_without_lang: str


# Page type -> page name without lang
PagesInfos = dict[str, str]


class EpisodeInfo(TypedDict):
    """V1 Webcomic info"""

    name: str
    pages: PagesInfos
    total_pages: int
    translated_languages: list[str]


class OldEpisodeInfo(TypedDict):
    """Old Webcomic info"""

    name: str
    total_pages: int
    translated_languages: list[str]


def filename_without_lang(page: Path) -> str:
    """Returns the filename of the provided path without leading lang info"""

    # page names follow this pattern
    #  [<lang>_]<ep name>_by-<artist name>_E<ep number>[P<page number>].<ext>

    # This means when spliting on "_", having a length of 3 means no LANG

    filename_parts = page.name.split("_")

    if len(filename_parts) == 3:
        return page.name

    return "_".join(filename_parts[1:])


def get_page_type(page: Path, credits_index: int) -> str:
    """
    Returns the type of the page:
    + "cover" if there is no page index in the name
    + "title" if page index is 0
    + "credits" if page index is equal to credit_index
    + The page index as string otherwise
    """

    # page names follow this pattern
    #  [<lang>_]<ep name>_by-<artist name>_E<ep number>[P<page number>].<ext>

    # Identifier is of form "E<ep number>" (cover) or "E<ep number>P<page number>"
    page_identifier = page.stem.rsplit("_", 1)[1]

    if "P" not in page_identifier:
        return "cover"

    page_index = int(page_identifier.split("P")[1])

    if page_index == 0:
        return "title"

    if page_index == credits_index:
        return "credits"

    return str(page_index)


def get_page_metadata(page: Path, credits_index: int) -> PageMetadata:
    """
    Extract metadata from the provided page
    pages_count is required to understand which page is the title and credit page
    """

    # page names are these (excluding parent directory/URI):
    #  comic pages (including title and credits): LANG_COMIC_by-ARTIST_E##P##.EXT
    #  cover pictures: LANG_COMIC_by-ARTIST_E##.EXT
    #  cover picture without text: COMIC_by-ARTIST_E##.EXT

    # Removing cover page and title page
    page_type = ""

    page_identifier = page.stem.rsplit("_", 1)[1]

    # Identifier is of form "E<ep number>" (cover) or "E<ep number>P<page number>"
    if "P" in page_identifier:
        page_index = int(page_identifier.split("P")[1])

        if page_index == 0:
            page_type = "title"

        elif page_index == credits_index:
            page_type = "credits"

        else:
            # backward convertion done to remove leading 0
            page_type = str(page_index)

    else:
        page_type = "cover"

    return {
        "page_type": page_type,
        "filename_without_lang": filename_without_lang(page),
    }


def get_pages_infos(episode_dir: Path) -> PagesInfos:
    """
    Generate pages metadata from an episode directory
    """

    pages_folder: Path = episode_dir / "low-res"

    pages_paths = sorted(pages_folder.glob("en*Pepper*.*"))
    pages_count = len(pages_paths)

    # Taking into account cover page (no index) and title page (index 0)
    credits_index = pages_count - 2

    pages_metadata: list[PageMetadata] = [
        get_page_metadata(page_path, credits_index) for page_path in pages_paths
    ]

    pages_infos: PagesInfos = {
        metadata["page_type"]: metadata["filename_without_lang"]
        for metadata in pages_metadata
    }

    return pages_infos


def generate_episodes_info(webcomics_dir: Path) -> list[EpisodeInfo]:
    """
    Generate a list of webcomic episode metadata from a webcomic directory.
    Episodes are sorted by name, which implicitly sort them by episode number.
    Within episode info, languages are sorted alphanumericaly.
    """

    result: list[EpisodeInfo] = []

    # This sorts episodes dirs paths alphanumericaly, which sorts them by episode number
    #   since it's the first element of the name : "ep<2 digit number>_<ep name>"
    # We are fine until episode 100, then the sorting will break unless all folders
    #   are renamed to a 3 digit number.
    episodes_dirs = sorted(webcomics_dir.glob("ep*_*/"))

    for episode_dir in episodes_dirs:
        info_file = episode_dir / 'info.json'

        # Check if the info.json file exists
        if info_file.is_file():
            with open(info_file) as f:
                try:
                    info_data = json.load(f)
                    # Check if "public" key exists and its value is 1
                    if info_data.get("public") == 1:
                        # Process the directory as it meets the criteria
                        print(f"[generate_json_episodes_lists.py] Processing directory: {episode_dir}")
                        langs_childrens = sorted(episode_dir.glob("lang/*"))
                        langs = [children.name for children in langs_childrens if children.is_dir()]

                        pages_infos = get_pages_infos(episode_dir)

                        # Page count ignores title and credit page (but includes cover)
                        episode_pages_count = len(pages_infos) - 2

                        episode_info: EpisodeInfo = {
                            "name": episode_dir.name,
                            "pages": pages_infos,
                            "total_pages": episode_pages_count,
                            "translated_languages": langs,
                        }

                        result.append(episode_info)

                except json.JSONDecodeError:
                    print(f"Error decoding JSON in {info_file}")


    return result


def as_old_info(webcomic_info: list[EpisodeInfo]) -> list[OldEpisodeInfo]:
    """
    Backports webcomic info to previous format.
    This remove all "pages" entries from pages info.
    """

    old_webcomic_info: list[OldEpisodeInfo] = []

    for episode_info in webcomic_info:

        old_episode_info: OldEpisodeInfo = {
            "name": episode_info["name"],
            "total_pages": episode_info["total_pages"],
            "translated_languages": episode_info["translated_languages"],
        }

        old_webcomic_info.append(old_episode_info)

    return old_webcomic_info


if __name__ == "__main__":

    # The first argument is the episodes directory that we will inspect
    webcomic_folder_path = Path(sys.argv[1])

    json_data = generate_episodes_info(webcomic_folder_path)

    # The second is the destination of the v1 data
    v1_json_path = Path(sys.argv[2])

    # write the data structure to stdout in JSON format
    with v1_json_path.open(mode="w", encoding="utf-8") as v1_json_file:
        json.dump(json_data, v1_json_file, indent=4, sort_keys=True)

    # If it exists, the third argument is the destination of the old data
    if len(sys.argv) > 3:
        old_json_path = Path(sys.argv[3])

        old_json_data = as_old_info(json_data)

        with old_json_path.open(mode="w", encoding="utf-8") as old_json_file:
            json.dump(old_json_data, old_json_file, indent=4, sort_keys=True)
