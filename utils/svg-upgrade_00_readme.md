SVG UPGRADE
===========

Three script in this repository can assist with automatisation
of converting Inkscape SVG from old to actual version. It was
designed for the transition from Inkscape 91 files to 92. 

Here are quick note on the workflow, as reminder:

1. Edit and execute svg-filewalker.sh
-------------------------------------

This script browse all the SVG of a target folder 
(can be lang in general or lang/en). It is possible
to plug the svg-upgrade_01.sh to launch on 
any SVG the script met.

2. Action of svg-upgrade_01.sh
------------------------------

This script will convert all the SVG to the current 
Inkscape version and will then render a preview in tmp, 
and then create a gif animation comparing:
- previous version  
- actual re-saved version  

The comparison has a test: if this one do no difference,
then the script validate the new version. 

But if the script detect differences, the script still
saves the SVG but add in the folder "refactor" at root
of project; the SVG to a list (bad-svg-list.txt) and the gif-animation itself.

Error of writing or saving are exported to the list 0_write-issue.txt
in refactor folder.

3. Review with svg-upgrade_02_validate-bad-svg.sh
-----------------------------------------------

This script can be launched directly from the terminal:  
It will simply browse the content of the "refactor" folder 
and use the bad-svg-list.txt file list.

A pop-up appear on the center of screen "is it OK?"
- Yes: remove diff (mv them to a trash subfolder), accept the SVG as it is
- No: open manually the Inkscape file for modification

A counter helps to see the progress.

4. Error fix with svg-upgrade_03_fix-write-issue.sh
---------------------------------------------------

This final script takes the content of 0_write-issue.txt and
tries to brut force the convertion. It often works. :-)

When all is done, you can remove the -refactor- folder.


