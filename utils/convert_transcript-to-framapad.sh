#!/bin/bash
#
#  convert-transcript-to-framapad.sh
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#  SPDX-FileCopyrightText: © 2023 David Revoy <info@davidrevoy.com>
#
#   Script for cleaning the Markdown transcript into a format easier to copy/paste on Framapad
#   $1 Full absolute path to the file with the filename of the transcript
#   $2 Absolute path to output (optional)
#   Usage: ./convert-transcript-to-framapad.sh </absolute/path/to/the/transcript-file.md> <optional/path/to/output>
#
#  Depency required: sed

# Load global configuration
export script_absolute_path="`dirname \"$0\"`"
source "${script_absolute_path}/../lib/header.sh"
function_load_config
function_dependency_check sed

# Menu
echo "${Blue}----------------------------------------------------------------------------------${Off}"
echo "${Blue}                 CONVERT TRANSCRIPT INTO A FRAMAPAD FORMAT                        ${Off}"
echo "${Blue}----------------------------------------------------------------------------------${Off}"

echo "${Yellow}----------------------------------------------------------------------------------${Off}"
echo " ${Green}Please enter an episode number (two digits):${Off}"
echo "${Yellow}----------------------------------------------------------------------------------${Off}"
echo -n "${Green}(eg. 07, 23 or 32) ?${Off}"
read EpisodeTarget

echo "${Yellow}----------------------------------------------------------------------------------${Off}"
echo " ${Green}Please enter a P&C ISO lang (two digits):${Off}"
echo "${Yellow}----------------------------------------------------------------------------------${Off}"
echo -n "${Green}(eg. en, fr or eo) ?${Off}"
read LangTarget

echo "Summary: Convert the transcript of EP${EpisodeTarget}, in ${LangTarget} lang."

# Guess an absolute path from data:
cd ${project_root}/${folder_webcomics}/*ep${EpisodeTarget}*
EpisodePath="${PWD}"

if [ -d ${EpisodePath}/lang/${LangTarget} ]; then

    cd ${EpisodePath}/lang/${LangTarget}
    TranscriptDirectory=${EpisodePath}"/lang/"${LangTarget}
    TrasncriptFilename="ep"${EpisodeTarget}"_"${LangTarget}"_transcript.md"

    if [ -f ${TranscriptDirectory}"/"${TrasncriptFilename} ]; then
    
        # If we got here, we checked existence of the path and the transcript.
        # It's safe to work on it now:
        TranscriptBasename=${TrasncriptFilename##*/}
        IsoDateToday=$(date +%Y-%m-%d_%Hh%Mm%Ss)
        OutputPadFile=${IsoDateToday}"_"${TranscriptBasename}"_pad.html"
        OutputDirectory=$HOME

        echo "${Blue}Converting ${TrasncriptFilename} --> ${OutputPadFile} :${Off}"
        echo "Dest. path: ${OutputDirectory}"

        # Writing the HTML5 UTF8 header for our document.
        echo "<!doctype html>" >> ${OutputDirectory}"/"${OutputPadFile}
        echo "<html>" >> ${OutputDirectory}"/"${OutputPadFile}
        echo "<head>" >> ${OutputDirectory}"/"${OutputPadFile}
        echo "  <link rel=\"stylesheet\" href="styles.css">" >> ${OutputDirectory}"/"${OutputPadFile}
        echo "  <meta charset=\"utf-8\">" >> ${OutputDirectory}"/"${OutputPadFile}
        echo "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">" >> ${OutputDirectory}"/"${OutputPadFile}
        echo "</head>" >> ${OutputDirectory}"/"${OutputPadFile}
        echo "<body>" >> ${OutputDirectory}"/"${OutputPadFile}
        echo "<pre>" >> ${OutputDirectory}"/"${OutputPadFile}

        echo "# Version: ${IsoDateToday}" >> ${OutputDirectory}"/"${OutputPadFile}
        cat ${TranscriptDirectory}"/"${TrasncriptFilename} >> ${OutputDirectory}"/"${OutputPadFile}

        # Start cleaning
        sed -i 's#True##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#<hidden>##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#<unknown>##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#False##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#|[0-9]|##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#|[0-9][0-9]|##g' ${OutputDirectory}"/"${OutputPadFile}

        sed -i 's#----|--------|-----------|----|---------------------##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#Name|Position|Concatenate|Text|Whitespace (Optional)##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#Providing transcripts is optional for translators.##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#The Pepper&Carrot site will work fine without them,##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#but they help for accessibility, screen readers and language learners.##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#Read https\:\/\/www.peppercarrot.com\/xx\/documentation\/062_Transcripts\.html##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#for how they can be generated without retyping them after you are done##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#translating or updating the content of the speechbubbles in the SVG(s)##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's#and more information and documentation about them.##g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's%## Notes%%g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's%## Pages%%g' ${OutputDirectory}"/"${OutputPadFile}

        sed -i 's%### P%--- Page %g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's%[a-zA-Z]*|%%g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's%--- Page 00%%g' ${OutputDirectory}"/"${OutputPadFile}

        sed -i 's% \.%.%g' ${OutputDirectory}"/"${OutputPadFile}
        sed -i 's% \,%,%g' ${OutputDirectory}"/"${OutputPadFile}

        sed -i '/^$/{:a;N;s/\n$//;ta}' ${OutputDirectory}"/"${OutputPadFile}

        echo " </pre>" >> ${OutputDirectory}"/"${OutputPadFile}
        echo "</body>" >> ${OutputDirectory}"/"${OutputPadFile}
        echo "</html>" >> ${OutputDirectory}"/"${OutputPadFile}

        echo "${Green}Done${Off}"

        # Mini service for convenience:
        firefox ${OutputDirectory}"/"${OutputPadFile}

    else
        echo "${Red}Error:${Off} Transcript ${EpisodePath}/lang/${LangTarget}/${TrasncriptFilename} not found. Aborting."
        exit
    fi

else
    echo "${Red}Error:${Off} Directories ${EpisodePath}/lang/${LangTarget} not found. Aborting."
    exit
fi

# Task is executed inside a terminal
# This line prevent terminal windows to be closed
# Necessary to read log later
echo -n " Press [Enter] to exit"
read end
exit
