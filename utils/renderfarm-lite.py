#!/usr/bin/env python3

import sys

# No risk using subprocess : commands are hardcoded and no user input is used
import subprocess  # nosec
from pathlib import Path


# Config

HELP_TEXT = """Usage: renderfarm-lite.py [LANGUAGE]...
Render Pepper&Carrot translations

This renderfarm lite only renders translations. It's perfect for translators or
translation reviewers who wish to render a whole episode without the fuss of Krita
source files and the likes.

How to use:
* Render all languages:
    $ cd comics/ep24    or    cd comics/ep24/lang
    $ ~/pc/tools/utils/renderfarm-lite.py

* Render English and French:
    $ cd comics/ep24    or    cd comics/ep24/lang
    $ ~/pc/tools/utils/renderfarm-lite.py en fr

* Render English:
    - either in the same way as above, or
    - $ cd comics/ep24/lang/en
      $ ~/pc/tools/utils/renderfarm-lite.py

You can a install symlink to the script in a directory in your PATH to be able to call
it without giving the full path, but that is outside the scope of this help text.

Original author: Midgard
License: GPLv3 or later
"""

INKSCAPE_COMMAND = "inkscape"


# File system and paths stuff


def is_newer(path_a: Path, path_b: Path) -> bool:
    """Check if path_a was more recently modified than path_b"""

    return path_a.stat().st_mtime > path_b.stat().st_mtime


def is_valid_lang_name(lang: str) -> bool:
    """
    Check if the provided string has a length between 2 and 3 and is lowercase alpha
    """

    return 2 <= len(lang) <= 3 and lang.isalpha() and lang.islower()


def is_valid_lang_dir(path: Path) -> bool:
    """
    Checks the name of the provided folder is a valid lang name and that it contains
    at least 3 svg files.
    """

    if not path.is_dir():
        return False

    if not is_valid_lang_name(path.name):
        return False

    svg_files = list(path.glob("*.svg"))

    if len(svg_files) < 3:
        return False

    return True


def lang_subdir_if_needed(directory: Path) -> Path:
    """
    Return a Path to the "lang" subfolder if it exists in "directory" and "directory"
    itself is not called "lang".
    Returns `directory` otherwise.
    """

    lang_subfolder = directory / "lang"

    if directory.name != "lang" and lang_subfolder.is_dir():
        return lang_subfolder

    return directory


# Rendering


def render_lang(source_dir: Path, dest_dir: Path) -> None:
    """
    Renders every SVGs from source_dir as PNG into dest_dir

    :param lang: language abbreviation, only for logging
    :param source_dir: path to directory with SVG files to render
    :param dest_dir: path to directory to put rendered images in
    """
    print(f"Rendering [{source_dir.name}]", flush=True)

    files_to_render = sorted(source_dir.iterdir())

    for filepath in files_to_render:

        if filepath.suffix == ".svg":

            dest_filepath = dest_dir / filepath.with_suffix(".png").name

            if not dest_filepath.is_file() or is_newer(filepath, dest_filepath):

                print(f" -> {dest_filepath}", flush=True)

                # No risk using subprocess : inkscape command is hardcoded
                # and script is run manually
                subprocess.run(  # nosec
                    (
                        INKSCAPE_COMMAND,
                        "--export-type=png",
                        f"--export-filename={dest_filepath}",
                        "--export-width=990",
                        filepath,
                    ),
                    check=True,
                )

            else:
                print(f" -> {dest_filepath} is up-to-date", flush=True)


def main() -> None:

    try:
        if sys.argv[1] == "-h" or sys.argv[1] == "--help":
            print(HELP_TEXT, file=sys.stderr)
            sys.exit(0)

    except IndexError:
        pass

    complain_invalid_langs = False

    directory: Path = Path.cwd()
    dir_childens: list[Path] = []

    # This if-else cascade is not particularly elegant, but I wanted
    # the script to detect several possible CWDs.

    # If there are arguments, always prefer those and build children paths from argv
    if len(sys.argv) > 1:
        directory = lang_subdir_if_needed(directory)

        dir_childens = [directory / name for name in sys.argv[1:]]

        complain_invalid_langs = True

    # If there are ≥3 SVGs in the CWD, assume we are in a language
    # directory and render it.

    elif len(list(directory.glob("*.svg"))) >= 3:
        dir_childens = [directory]
        directory = directory.parent

    # If cwd isn't called "lang" but there's a subdir "lang", start
    # from that subdir.
    else:
        directory = lang_subdir_if_needed(directory)
        dir_childens = [entry for entry in directory.iterdir() if entry.is_dir()]

    destination_dir = directory.parent / "render"

    count_langs_done = 0

    for child in dir_childens:

        lang_dest = destination_dir / child.name

        if is_valid_lang_dir(child):
            lang_dest.mkdir(parents=True, exist_ok=True)

            render_lang(child, lang_dest)
            count_langs_done += 1

        elif complain_invalid_langs:
            print(
                f"{child.name} is not a valid language abbreviation, "
                "the languages' directory does not exist, "
                "or it doesn't contain SVG files.",
                file=sys.stderr,
            )

    if count_langs_done == 0:
        print("Nothing to render detected.", file=sys.stderr)
    else:
        print(f"Rendered episode in {count_langs_done} languages")


if __name__ == "__main__":
    main()
