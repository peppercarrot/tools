#!/bin/bash

# Load global configuration
export script_absolute_path="`dirname \"$0\"`"
source "${script_absolute_path}/../lib/header.sh"
function_load_config
function_dependency_check convert

# Input parameters
cd ${project_root}/${folder_webcomics}/new-*
NewEpisodePath="${PWD}"

# Menu
echo "${Yellow}----------------------------------------------------------------------------------${Off}"
echo "${Yellow}                 OPTIMIZE PNG IN LANG FOLDER OF THE NEW EPISODE                   ${Off}"
echo "${Yellow}----------------------------------------------------------------------------------${Off}"
echo " ${Green}1${Off} --> ${Green}Compressed${Off} Size: 640px, 64 colors: ideal for alpha/beta"
echo " ${Green}2${Off} --> ${Green}Full Resolution${Off} Original size, full colors: ideal for fine adjustements"
echo " ${Green}3${Off} --> ${Green}Regular${Off} Size: 992px, full colors: for final storage on Git"
echo "${Yellow}----------------------------------------------------------------------------------${Off}"
echo -n "${Green}your choice ( 1, 2 or 3 ) ?${Off}"
read CHOICE
echo " your selection is $CHOICE"

if [ -d ${NewEpisodePath}/lang ]; then

    cd ${NewEpisodePath}/lang

    for pngfile in *.png; do

        echo "${Red} $pngfile (before):"
        du -k ${pngfile} | cut -f 1
        echo "${Off}"

        if [ "$CHOICE" = "1" ]; then
          convert ${NewEpisodePath}/hi-res/gfx-only/lossless/${pngfile} -resize 640x -colors 64 ${NewEpisodePath}/lang/${pngfile}
          optipng -o7 ${NewEpisodePath}/lang/${pngfile}
        elif [ "$CHOICE" = "2" ]; then
          cp ${NewEpisodePath}/hi-res/gfx-only/lossless/${pngfile} ${NewEpisodePath}/lang/${pngfile}
        elif [ "$CHOICE" = "3" ]; then
          convert ${NewEpisodePath}/hi-res/gfx-only/lossless/${pngfile} -resize 992x ${NewEpisodePath}/lang/${pngfile}
          optipng -o7 ${NewEpisodePath}/lang/${pngfile}
        fi

        echo "${Green} $pngfile (after):"
        du -k ${pngfile} | cut -f 1
        echo "${Off}"

    echo ""
    echo "------------------"
    echo ""
    done

fi

# Task is executed inside a terminal
# This line prevent terminal windows to be closed
# Necessary to read log later
echo -n " Press [Enter] to exit"
read end
