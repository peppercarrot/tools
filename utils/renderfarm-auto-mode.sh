#!/bin/bash
#
#  renderfarm-auto-mode.sh
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#  SPDX-FileCopyrightText: © 2023 David Revoy <info@davidrevoy.com>
#
#  Script designed to be launched on a computer with all the renderfarm setup.
#  The script will check any changes to webcomic repository. If it is the case,
#  A render of the full project will be initiated, and then an upload of the files
#  to the website will happens. 
#  This script was designed when I'm not home to manage the rendering of SVGs and
#  push them. It makes any changes to Git~master a bit more dangerous, but should
#  be OK(TM).

# Load global configuration
export script_absolute_path="`dirname \"$0\"`"
source "${script_absolute_path}/../lib/header.sh"
function_load_config 

# Information
export script_title="RENDERFARM AUTO MODE"
export script_version="0.1a"
export script_filename="renderfarm-auto-mode.sh"

# Script
function_decorative_header

# Carrot Ascii-art badge
echo " ${Yellow}${PurpleBG}                                   ${Off}"
echo " ${Yellow}${PurpleBG}      [---------]                  ${Off}"
echo " ${Yellow}${PurpleBG}      |   BOT   |     ( YAY ! )    ${Off}"
echo " ${Yellow}${PurpleBG}      |---------|      /           ${Off}"
echo " ${Yellow}${PurpleBG}    ==|  o   o  |==                ${Off}"
echo " ${Yellow}${PurpleBG}      |    u    |                  ${Off}"
echo " ${Yellow}${PurpleBG}      [---------]                  ${Off}"
echo " ${Yellow}${PurpleBG}                                   ${Off}"
echo ""

# Target to check on changes
cd "$project_root"/"$folder_webcomics"

# Delay before running another loop.
# in second, 3600 = 1h
SLEEPTIME=3600

echo " * Checking changes on "$project_root"/"$folder_webcomics""

while ( true ); do

    # Update a precise print of the time for the log in all loops.
    PRECISETIME=$(date "+%Y-%m-%d_%Hh%Mm%Ss")

    # Ask Git to check for news from remote
    git fetch

    # Get the git hash of our local repository
    LOCALHASH=$(git rev-parse HEAD)

    # Get the git hash of our upstream repository (distant, origin~master)
    UPSTREAMHASH=$(git rev-parse master@{upstream})

    # Compare them:
    if [ "$LOCALHASH" != "$UPSTREAMHASH" ]; then
        echo "${Green} * [${PRECISETIME}] : I found update!${Off}";

        # Run the renderfarm in non-prompt mode.
        bash "$project_root"/"$folder_tools"/update-all.sh

        # Menu
        echo "${Blue}----------------------------------------------------------------------------------${Off}"
        echo "${Blue}               [${PRECISETIME}]:  RENDERFARM TASK ARE DONE                        ${Off}"
        echo "${Blue}----------------------------------------------------------------------------------${Off}"
    else
        echo " * [${PRECISETIME}] : Nothing to do, but I'm still checking. I'll sleep for the next ${SLEEPTIME} seconds.";
    fi

    # Wait before starting another loop
    sleep ${SLEEPTIME};

done



