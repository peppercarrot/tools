#!/bin/bash
# A wrapper to quickly call in a terminal 0_transcripts/validate-transcripts.sh 
# Located inside the webcomic repository


# Information
export script_title="Validate Transcript (Wrapper)"
export script_version="0.1a"
export script_filename="validate-transcript-wrapper.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config

# Script
function_decorative_header

if [ -f "$project_root"/"$folder_webcomics"/0_transcripts/validate-transcripts.sh ]; then
	cd "$project_root"/"$folder_webcomics"
	0_transcripts/validate-transcripts.sh
    git status
else
	echo "${Red}* Error:${Off} ${project_root}/${folder_webcomics}/0_transcripts/validate-transcripts.sh not found."
fi

# Footer library
function_load_source_lib "$script_absolute_path"/lib/footer.sh

if [ "$1" = "--prompt" ]; then
# Task is executed inside a terminal window (pop-up)
# This line prevent terminal windows to be auto-closed
# and necessary to read log later, or to reload the script
 echo "${Purple}  Press [Enter] to exit or [r](then Enter) to reload $script_filename${Off}"
 read -p "?" ANSWER
 if [ "$ANSWER" = "r" ]; then
   "$project_root"/"$folder_tools"/"$script_filename" --prompt
 fi
fi
