#!/bin/bash
#: Title       : miniFantasyTheater renderfarm
#: Author      : David REVOY < info@davidrevoy.com >
#: License     : GPLv3 or higher

# Check if a path is provided
if [ -z "$1" ]; then
  echo "[check] ERROR: No argument found."
  echo "        Usage: $0 <target> --options"
  echo "        "
  echo "        <target>: absolute path to the directory containing the Krita files"
  echo "        options:"
  echo "                --noprompt : don't prompt at the end a CLI gui to reload the script."
  echo "        "
  exit 1
fi

# Get the path from the argument
export dirPath="$1"

# Check if the path exists, and if it is a valid directory
if [ -d "$dirPath" ]; then
  echo "[check] $dirPath exists." # debug
  if ls "$dirPath"/*.kra 1> /dev/null 2>&1; then
    echo "[check] $dirPath contains Krita files." # debug
    if [ -f "$dirPath"/.renderConfig.sh ]; then
      echo "[check] "$dirPath"/.renderConfig.sh found. Loading."
      source "$dirPath"/.renderConfig.sh
    else
      echo "[check] "$dirPath"/.renderConfig.sh not found. Loading default."
      # Default config
      # --------------
      # User configuration:
      export defaultLang="en"
      export projectName="Unnamed Project (no custom .RenderConfig.sh file found)"
      export creditSuffix=""
      export defaultAttribution="Unamed <https://not.a/real/link.html>"
      # An absolute path to a directory (ending by /) to save copyt of Krita and SVG files at each rendering.
      # (eg. here the path of my external "Virgo" disk)
      export backupPath="/tmp/"
      # Export:
      export lowResSize="1200x" # width in px for the resulting JPG in low-res directory
      export lowResPpi="96" # in ppi for the JPG outputs Classic values: 72
      export hiResPpi="300" # in ppi for your Krita files in order to get real size in mm 
      export multipage=1 # 1→ It's a multi page episode in a directory, 2→ each picture in the directory are standalone episodes.
    fi
  else
    echo "[check] ERROR: $dirPath is not a directory with Krita files in it."
    exit 1
  fi
else
  echo "[check] ERROR: $dirPath does not exist."
  exit 1
fi

# Title
printf "\033]0;%s\007\n" "$projectName renderfarm"

# Directories name
export langDir="lang"
export lowResDir="low-res"
export hiResDir="hi-res"
export gfxOnlyDir="gfx-only"
export txtOnlyDir="txt-only"
export thumbDir="thumbnails"
export wipDir="wip"
export cacheDir="cache"

version=$(date +%Y-%m-%d_%Hh%M%S)
export version
export Blue=$'\e[1;34m'
export Yellow=$'\e[1;33m'
export Green=$'\e[1;32m'
export Red=$'\e[1;31m'
export Purple=$'\e[1;35m'
export Off=$'\e[0m'

# A better mkdir
_makeDir (){
  if [ ! -d "$1" ]; then
    echo "* create new directory: $1"
    mkdir -p "$1"
  fi
}

# A safer rm
_deleteSecure () {
  pathToDelete=$1
  # exit if empty
  if [ -z "${pathToDelete}" ]; then
    echo "${Red} security exit: a variable for deleting is undefined (dangerous).${Off}"
    exit 1
  fi

  if [ -f "${pathToDelete}" ]; then
    rm "${pathToDelete}"
  fi

  if [ -d "${pathToDelete}" ]; then
    rm -r "${pathToDelete}"
  fi
}

# Check if dependencies $1, $2 ,$3 ... exist.
_dependency_check () {
    for arg; do
      if command -v "$arg" >/dev/null 2>&1 ; then
        true
      else
        echo "${Red}* Error:${Off} missing dependency: ${Red}$arg${Off} not found."
        echo "Fatal: missing dependency: $arg."
        exit
      fi
    done
}

# Library to check on Inkscape rendering (because it is not reliable at all)
_inkscapeRender () {
  input=$1
  ouput=$2

  if [ ! -f "${input}" ]; then
    echo "Error: target $input does not exist. Inkscape can't render a non existing SVG. Cancelling render"
    break
  fi

  # workaround two:
  inkscape "${input}" -o "${ouput}" > /dev/null
  sleep 4
  if [ ! -f "${ouput}" ]; then
    echo "Rendering failed, retrying."
    inkscape "${input}" -o "${ouput}" > /dev/null
    if [ ! -f "${ouput}" ]; then
      echo "Rendering failed a second time, retrying."
      inkscape "${input}" -o "${ouput}" > /dev/null
      if [ ! -f "${ouput}" ]; then
        echo "Rendering failed a third time, retrying a last one, after 3 seconds."
        sleep 3
        inkscape "${input}" -o "${ouput}" > /dev/null
        if [ ! -f "${ouput}" ]; then
          echo "${Red}--------------------------------------------------------------- ${Off}"
          echo "${Red}Error: Inkscape couldn't render something even after three time.${Off}"
          echo "${Red}input -> $1 ${Off}"
          echo "${Red}output -> $2 ${Off}"
          echo "${Red}--------------------------------------------------------------- ${Off}"
        fi
      else
        echo "Success: Inkscape render (3rd time)."
      fi
    else
      echo "Success: Inkscape render (2nd time)."
    fi
  fi
}

_renderKraComicPages(){
  inFile="$1" # file gets in with an absolute path

  inFilename=${inFile##*/}
  kraFile=${inFilename}
  kraFileNoExt="${kraFile%.*}"

  txtFile="${kraFileNoExt}.txt"
  pngFile="${kraFileNoExt}.png"
  svgFile="${kraFileNoExt}.svg"
  jsonFile=".${kraFileNoExt}.json"
  jpgFile="${kraFileNoExt}.jpg"
  kraTmpDir="${kraFileNoExt}-${version}"
  jpgFileWip="${kraFileNoExt}_${version}.jpg"
  renderMeFile="${kraFileNoExt}-renderme.txt"
  jpgFileCredit="${kraFileNoExt}${creditSuffix}.jpg"
  jpgFileThumbnail="${kraFileNoExt}-thumb${creditSuffix}.jpg"

  # Checksum of *.kra file.
  kraMd5sum=$(md5sum "${dirPath}/${kraFile}")

  # Create a tokken if none is available, so later grep can compare with empty file.
  if [ ! -f "${dirPath}/${cacheDir}/${txtFile}" ]; then
    touch "${dirPath}/${cacheDir}/${txtFile}"
  fi

  # Compare if actual *.kra checksum is similar to the previous one recorded on txtFile.
  if grep -q "${kraMd5sum}" "${dirPath}/${cacheDir}/${txtFile}"; then
    # Nothing to do, kraFile is up-to-date.
    true
  else
    echo "${Yellow}[${kraFile}] -> Rendering. ${Off}"

    # Save a token to inform the SVG renderer later to re-render the txt for this page.
    touch "${dirPath}/${cacheDir}/${renderMeFile}"

    # Update the cache.
    md5sum "${dirPath}/${kraFile}" > "${dirPath}/${cacheDir}/${txtFile}"

    # Extract the hi-resolution PNG directly from *.kra file.
    mkdir -p "/tmp/${kraTmpDir}"
    unzip -j "${dirPath}/${kraFile}" "mergedimage.png" -d "/tmp/${kraTmpDir}" > /dev/null
    # Sanify the PNG (no Alpha, compression, sRGB colorspace)
    convert "/tmp/${kraTmpDir}/mergedimage.png" -define colorspace:auto-grayscale=false -define png:color-type=2 -colorspace sRGB -background white -alpha remove -define png:compression-strategy=3 -define png:compression-level=9 "${dirPath}/${cacheDir}/${pngFile}"

    # Update the preview in the langDir, for translation
    convert "${dirPath}/${cacheDir}/${pngFile}" -colorspace sRGB  -resize "${lowResSize}" -density "${lowResPpi}" -units pixelsperinch -unsharp 0x0.50+0.50+0 -quality 85% "${dirPath}/${langDir}/${jpgFile}"

    # Update the work in progress (wip) snapshot.
    convert "${dirPath}/${cacheDir}/${pngFile}" -colorspace sRGB -density "${hiResPpi}" -quality 95% "${dirPath}/${wipDir}/${jpgFileWip}"

    # Copy a version to the graphic only directory
    cp -rf "${dirPath}/${wipDir}/${jpgFileWip}" "${dirPath}/${hiResDir}/${gfxOnlyDir}/${jpgFileCredit}"

    if [ $multipage == 2 ]; then
      # Multipage episode require a gallery.
      # This thumbnailer crops a quarter of the picture, for the first panel into a 2x2 comic strip.
      convert "${dirPath}/${cacheDir}/${pngFile}" -colorspace sRGB -crop 1712x1712+171+162 -resize "${lowResSize}" -density "${lowResPpi}" -units pixelsperinch -unsharp 0x0.50+0.50+0 -quality 89% "${dirPath}/${lowResDir}/${thumbDir}/${jpgFileThumbnail}"
    fi

    # Copy the file to create a backup of the current version.
    cp "${dirPath}/${kraFile}" "${backupPath}/${version}_${kraFile}"

    # Get size info of the original
    imgWidth=$(identify -format "%w" "${dirPath}/${cacheDir}/${pngFile}")> /dev/null
    imgHeight=$(identify -format "%h" "${dirPath}/${cacheDir}/${pngFile}")> /dev/null

    # Remove the tmp folder.
    _deleteSecure "/tmp/${kraTmpDir}"

    # SVG template for translation
    if [ ! -f "${dirPath}/${langDir}/${defaultLang}/${svgFile}" ]; then
      cat > "${dirPath}/${langDir}/${defaultLang}/${svgFile}" << EOL
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   id="svg2"
   version="1.1"
   inkscape:version="0.1.3 (bash-generated)"
   width="${imgWidth}"
   height="${imgHeight}"
   sodipodi:docname="page.svg">
  <metadata
     id="metadata8">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <defs
     id="defs6" />
  <sodipodi:namedview
     pagecolor="#444444"
     bordercolor="#000000"
     borderopacity="1"
     objecttolerance="10"
     gridtolerance="10"
     guidetolerance="10"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:window-width="1920"
     inkscape:window-height="1050"
     id="namedview4"
     showgrid="false"
     inkscape:zoom="0.15037482"
     inkscape:cx="-900.65922"
     inkscape:cy="1491.8983"
     inkscape:window-x="1920"
     inkscape:window-y="0"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1" />
  <g
     inkscape:groupmode="layer"
     id="layer2"
     inkscape:label="artwork"
     sodipodi:insensitive="true">
     <image
       xlink:href="../${jpgFile}"
       y="0"
       x="0"
       id="${kraFileNoExt}"
       height="${imgHeight}"
       width="${imgWidth}" />
  </g>
  <g
     inkscape:groupmode="layer"
     id="layer3"
     inkscape:label="speechbubbles" />
  <g
     inkscape:groupmode="layer"
     id="layer1"
     inkscape:label="txt" />
</svg>
EOL
    fi

    # Database file template
    if [ $multipage == 1 ]; then
      jsonFile="info.json"
    fi
    if [ ! -f "${dirPath}/${jsonFile}" ]; then
      currentYear=$(date +%Y)
      cat > "${dirPath}/${jsonFile}" << EOL
{
  "published": "$currentYear-00-00",
  "original-language": "en",
  "pluxmlID": 0,
  "mastodonID": 0,
  "background-color": "#FFFFFF",
  "license": "cc-by",
  "public": 0,
  "related-urls": [
        ""
  ],
  "credits": {
    "art":[
      "$defaultAttribution"
    ],
    "scenario":[
      "$defaultAttribution"
    ]
  }
}
EOL
    fi
    if [ ! -f "${dirPath}/.renderConfig.sh" ]; then
      currentYear=$(date +%Y)
      cat > "${dirPath}/.renderConfig.sh" << EOL
#!/bin/bash

# Default config
# --------------
# User configuration:
export defaultLang="en"
export projectName="Unnamed Project (no custom .RenderConfig.sh file found)"
export creditSuffix=""
export defaultAttribution="Unamed <https://not.a/real/link.html>"
# An absolute path to a directory (ending by /) to save copyt of Krita and SVG files at each rendering.
# (eg. here the path of my external "Virgo" disk)
export backupPath="/tmp/"
# Export:
export lowResSize="1200x" # width in px for the resulting JPG in low-res directory
export lowResPpi="96" # in ppi for the JPG outputs Classic values: 72
export hiResPpi="300" # in ppi for your Krita files in order to get real size in mm 
export multipage=1 # 1→ It's a multi page episode in a directory, 2→ each picture in the directory are standalone episodes.
EOL
    fi

  fi
}

_parralalComputeKra()
{
  export -f _renderKraComicPages
  export -f _deleteSecure
  find "${dirPath}"/*.kra -maxdepth 1 | parsort | parallel --keep-order _renderKraComicPages "{}"
}

_renderSvgComicPages(){
  inFile="$1" # file gets in with an absolute path

  inPath=${inFile%/*}
  svgFile=${inFile##*/}
  svgFileNoExt="${svgFile%.*}"

  lang=${inPath//"${dirPath}/${langDir}/"/}
  microtime=$(date +%s)
  svgTmpDir="${svgFileNoExt}-${microtime}"
  pngFile="${svgFileNoExt}.png"
  pngFileCredit="${svgFileNoExt}${creditSuffix}.png"
  pdfFileCredit="${svgFileNoExt}${creditSuffix}.pdf"
  pngFileSanified="${svgFileNoExt}_sanified.png"
  txtFile="${svgFileNoExt}_${lang}.txt"
  renderMeFile="${svgFileNoExt}-renderme.txt"
  jpgFile="${svgFileNoExt}.jpg"
  jpgFileCredit="${svgFileNoExt}${creditSuffix}.jpg"
  transcriptFile="${svgFileNoExt}_transcript.md"
  jsonFile="${svgFileNoExt}_info.json"
  
  # workaround one: a bug: https://gitlab.com/inkscape/inkscape/-/issues/4716#note_1898150983
  # (not very effective)
  microtime=$(date +%s)
  export SELF_CALL=$microtime

  # Checksum of *.svg file.
  svgMd5sum=$(md5sum "${inFile}")

  # Create a tokken if none is available, so later grep can compare with empty file.
  if [ ! -f "${dirPath}/${cacheDir}/${txtFile}" ]; then
    touch "${dirPath}/${cacheDir}/${txtFile}"
  fi

  # Compare if actual *.svg checksum is similar to the previous one recorded on txtFile.
  if grep -q "${svgMd5sum}" "${dirPath}/${cacheDir}/${txtFile}"; then
    # Nothing to do, svgFile is up-to-date.
    true
  else
    touch "${dirPath}/${cacheDir}/${renderMeFile}"
  fi

  # Check if there is not a ready made renderme token ready
  if [ ! -f "${dirPath}/${cacheDir}/${renderMeFile}" ]; then
    true
  else
    echo "${Yellow}[${lang}][${svgFile}] -> Rendering. ${Off}"

    # Update the cache.
    md5sum "${inFile}" > "${dirPath}/${cacheDir}/${txtFile}"

    # Render the sRGB composited (text and artworks) full resolution PNG
    mkdir -p "/tmp/${svgTmpDir}/${lang}"
    cp -rf "${inFile}" "/tmp/${svgTmpDir}/${lang}/${svgFile}"
    sed -i 's/.jpg"/.png"/g' "/tmp/${svgTmpDir}/${lang}/${svgFile}"
    # To debug ↑
    #cat "/tmp/${svgTmpDir}/${lang}/${svgFile}" | grep xlink:href
    cp -rf "${dirPath}/${cacheDir}/${pngFile}" "/tmp/${svgTmpDir}/${pngFile}"

    _inkscapeRender "/tmp/${svgTmpDir}/${lang}/${svgFile}" "/tmp/${svgTmpDir}/${lang}/${pngFile}"
    convert "/tmp/${svgTmpDir}/${lang}/${pngFile}" -colorspace sRGB -background white -alpha remove -define png:compression-strategy=3 -define png:compression-level=9 "/tmp/${svgTmpDir}/${lang}_${pngFileSanified}"

    # Render an high resolution jpg
    convert "/tmp/${svgTmpDir}/${lang}_${pngFileSanified}" -colorspace sRGB -density "${hiResPpi}" -units pixelsperinch "${dirPath}/${hiResDir}/${lang}_${jpgFileCredit}"

    # Render a low-res version to share on the web
    convert "/tmp/${svgTmpDir}/${lang}_${pngFileSanified}" -resize "${lowResSize}" -density "${lowResPpi}" -units pixelsperinch -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 89% "${dirPath}/${lowResDir}/${lang}_${jpgFileCredit}"

    # Render a transparent raster PNG version of the text
    cp -rf "/tmp/${svgTmpDir}/${lang}/${svgFile}" "/tmp/${svgTmpDir}/${lang}/${svgFileNoExt}_backgroundless.svg"
    sed -i 's/<image[^<]*//' "/tmp/${svgTmpDir}/${lang}/${svgFileNoExt}_backgroundless.svg"
    _inkscapeRender "/tmp/${svgTmpDir}/${lang}/${svgFileNoExt}_backgroundless.svg" "${dirPath}/${hiResDir}/${txtOnlyDir}/${lang}_${pngFileCredit}"

    # Render a transparent vector PDF version of the text
    _inkscapeRender "/tmp/${svgTmpDir}/${lang}/${svgFileNoExt}_backgroundless.svg" "${dirPath}/${hiResDir}/${txtOnlyDir}/${lang}_${pdfFileCredit}"
    if [ ! -f "${dirPath}/${hiResDir}/${txtOnlyDir}/${lang}_${pdfFileCredit}" ]; then
        _inkscapeRender "/tmp/${svgTmpDir}/${lang}/${svgFileNoExt}_backgroundless.svg" "${dirPath}/${hiResDir}/${txtOnlyDir}/${lang}_${pdfFileCredit}"
    fi

    # Calculate real size of the Krita files, based on pixel size and resolution 
    # Get size of a full resolution render
    pngWidth=$(identify -format "%w" "${dirPath}/${hiResDir}/${lang}_${jpgFileCredit}")> /dev/null
    pngHeight=$(identify -format "%h" "${dirPath}/${hiResDir}/${lang}_${jpgFileCredit}")> /dev/null
    echo "Picture geometry: $pngWidth x $pngHeight px @ $hiResPpi ppi"
    # (scale=2 is for bc to do the math at only two decimal after floating point).
    printWidth=$(echo "scale=2; ($pngWidth / $hiResPpi) * 25.4" | bc)
    echo "Auto width for print: $printWidth mm"
    printHeight=$(echo "scale=2; ($pngHeight / $hiResPpi) * 25.4" | bc)
    echo "Auto height for print: $printHeight mm"
    pdfjam --outfile "${dirPath}/${hiResDir}/${txtOnlyDir}/${lang}_${pdfFileCredit}" --papersize "{${printWidth}mm,${printHeight}mm}" "${dirPath}/${hiResDir}/${txtOnlyDir}/${lang}_${pdfFileCredit}" > /dev/null 2>&1
    # For debugging size:
    echo "Rendered PDF size info:"
    pdfinfo "${dirPath}/${hiResDir}/${txtOnlyDir}/${lang}_${pdfFileCredit}" | grep 'Page size' | awk '{printf "Page size: %.2f x %.2f mm\n", $3*25.4/72, $5*25.4/72}'

    # Copy the file to create a backup of the current version.
    cp "${dirPath}/${langDir}/${lang}/${svgFile}" "${backupPath}/${version}_${svgFile}"

    _deleteSecure "${dirPath}/${cacheDir}/${renderMeFile}"
    _deleteSecure "/tmp/${svgTmpDir}"

    # Transcript (markdown) template
    if [ $multipage == 1 ]; then
      transcriptFile="transcript.md"
      jsonFile="info.json"
    fi
    if [ "${lang}" = "${defaultLang}" ]; then
      if [ ! -f "${dirPath}/${langDir}/${defaultLang}/${transcriptFile}" ]; then
      cat > "${dirPath}/${langDir}/${defaultLang}/${transcriptFile}" << EOL
# Untitled

Work in progress: no transcript available yet.

If you want to contribute writing it, the source file should be in a $langDir/$lang/$transcriptFile [somewhere down here](https://framagit.org/peppercarrot/webcomics/-/tree/master?ref_type=heads). Thank you.

EOL
      fi
    fi

    # Credit/Attribution JSON file template
    if [ ! -f "${dirPath}/${langDir}/${lang}/${jsonFile}" ]; then
      currentYear=$(date +%Y)
      cat > "${dirPath}/${langDir}/${lang}/${jsonFile}" << EOL
{
  "credits": {
    "translation":[
      "Name <https://framagit.org/URL>"
    ]
  },
  "intro": "",
}
EOL
    fi

  fi
}

_parralalComputeSvg()
{
  export -f _renderSvgComicPages
  export -f _deleteSecure
  export -f _inkscapeRender
  find "${dirPath}/${langDir}"/*/*.svg | parsort | parallel --keep-order --delay 0.1 _renderSvgComicPages "{}"
}


echo "${Blue}--------------------------------${Off}"
echo "${Blue} Rendering: $projectName ${Off}"
echo "${Blue}--------------------------------${Off}"

echo ""
_dependency_check pdfjam convert inkscape md5sum

echo "* target path: ${dirPath}"
_makeDir "${dirPath}/${langDir}"
_makeDir "${dirPath}/${langDir}/${defaultLang}"
_makeDir "${dirPath}/${lowResDir}"
if [ $multipage == 2 ]; then
  _makeDir "${dirPath}/${lowResDir}/${thumbDir}"
fi
_makeDir "${dirPath}/${hiResDir}"
_makeDir "${dirPath}/${hiResDir}/${gfxOnlyDir}"
_makeDir "${dirPath}/${hiResDir}/${txtOnlyDir}"

_makeDir "${dirPath}/${wipDir}"
_makeDir "${dirPath}/${cacheDir}"

echo ""
echo "${Blue}Krita Files:${Off}"
_parralalComputeKra

echo ""
echo "${Blue}Svg Files:${Off}"
_parralalComputeSvg

echo "${Blue}Done.${Off}"

# Debug: running cache-less (render all each time, for testing purpose).
#_deleteSecure "${dirPath}"/"${cacheDir}"

# Invite to reload the script. Bypass if called with --noprompt flag.
if [ "$2" = "--noprompt" ]; then
  echo ""
else
  echo ""
  echo "${Purple}  Press [Enter] to launch the script again or:"
  echo "[q]${Off} => to Quit.${Off}"
  read -r -p "?" ANSWER
  if [ "$ANSWER" = "q" ]; then
    exit
  else
    # Launch again
    bash "$0" "$1"
  fi
fi
