#!/bin/bash

# Information
export script_title="Wiki & Documentation update"
export script_version="2.0a"
export script_filename="wiki.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config
function_dependency_check convert git lftp

# Script
function_decorative_header

function_decorative_title "GIT UPDATE"
function_git_update "$project_root"/"$folder_wiki"
function_git_update "$project_root"/"$folder_doc"

function_decorative_title "FTP UPLOAD"
function_ftp_upload "$project_root"/"$folder_wiki" "/www/data/wiki/"
function_ftp_upload "$project_root"/"$folder_doc" "/www/data/documentation/"

function_decorative_title "UPDATE CACHE"
echo "* Regenerate last_updated.txt"
date +'%s' > "$project_root"/"$folder_webcomics"/last_updated.txt
date +'%d/%m/%Y%t%H:%M:%S' >> "$project_root"/"$folder_webcomics"/last_updated.txt
cat "$project_root"/"$folder_webcomics"/last_updated.txt
rsync -avzhe ssh --progress \
       --rsh="sshpass -p $server_password ssh -o StrictHostKeyChecking=no"  \
       "$project_root"/"$folder_webcomics"/last_updated.txt "$server_username@$server_host_adress":"www/0_sources" \
       | grep -E --color=never '^deleting|[^/]$'

echo "File transfert end." | tee -a "$log_file"

# Footer library
function_load_source_lib "$script_absolute_path"/lib/footer.sh

if [ "$1" = "--prompt" ]; then
# Task is executed inside a terminal window (pop-up)
# This line prevent terminal windows to be auto-closed
# and necessary to read log later, or to reload the script
 echo "${Purple}  Press [Enter] to exit or [r](then Enter) to reload $script_filename${Off}"
 read -p "?" ANSWER
 if [ "$ANSWER" = "r" ]; then
   "$project_root"/"$folder_tools"/"$script_filename" --prompt
 fi
fi
