#!/bin/bash

# Information
export script_title="Pepper&Carrot main menu"
export script_version="2.0a"
export script_filename="menu.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config
function_dependency_check convert zenity lftp "$text_editor" "$file_browser"

cd "$project_root"/"$folder_webcomics"

# Function to populate and display the main menu listing with list of episodes and main options.
function_main_menu () {
  
  items+=( "‣ Render all" )
  items+=( "‣ Uploader" )
  items+=( " " )
  for directories in $(find $project_root/$folder_webcomics -maxdepth 1 -type d -printf '%f\n' | sort -r ); do
    episodefolder=$directories
  done
  for directories in $(find $project_root/$folder_webcomics -maxdepth 1 -type d -printf '%f\n' | sort -r ); do
    episodefolder=$directories
    if [[ $episodefolder == "ep"[0-9][0-9]_* ]]; then
      items+=( "$episodefolder"* )
    fi
  done
  items+=( "miniFantasyTheater" )
  for directories in $(find $project_root/$folder_webcomics/misc -maxdepth 1 -type d -printf '%f\n' | sort -r ); do
    episodefolder=$directories
    if [[ $episodefolder == [0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_* ]]; then
      items+=( misc/"$episodefolder" )
    fi
  done
  items+=( "+ Add a new episode" )
  items+=( " " )
  items+=( "‣ Website" )
  items+=( "‣ Wiki & Doc" )
  items+=( "‣ Community update" )
  items+=( "‣ Validate transcript" )
  items+=( "‣ Regenerate episodes.json manually" )
  items+=( "‣ Optimize the PNG of the new- episode" )
  items+=( "‣ Convert a Transcript to a Pad" )

  menuchoice=$(zenity --list --title="$script_title" \
              --width=400 --height=600 --window-icon="$folder_tools/lib/new-episode_template/img/peppercarrot_icon.png" \
              --text='Select an episode or an action' \
              --column='menu' "${items[@]}");

  # Method to avoid a Zenity bug with trailing | on option selected
  clear
  menuchoicecleaned=${menuchoice%|*}
}

# Function to populate and display the sub menu when a episode is selected with episode options
function_launch_render() 
{
  echo "=> rendering script"
  echo "$project_root"/"$folder_webcomics"/"$menuchoicecleaned"/
  if [[ $menuchoicecleaned == "ep"[0-9][0-9]_* ]]; then
    function_run_in_terminal "$project_root/$folder_tools/renderfarm.sh $project_root/$folder_webcomics/$menuchoicecleaned/" &
  else
    function_run_in_terminal "$project_root/$folder_tools/mft_renderfarm.sh $project_root/$folder_webcomics/$menuchoicecleaned/" --noprompt &
  fi
}

# Script
function_main_menu

# Analyse the output string of Zenity main menu and adapt behavior for each case.
if [ "$menuchoicecleaned" = " " ]; then
  echo "${Green}* mistake!!  ${Off}"
  zenity --error --text "You selected a spacer, try again."; echo $?
    
elif [ "$menuchoicecleaned" = "" ]; then
  # OK or Cancel is pressed without selection, we exit.
  exit

elif [ "$menuchoicecleaned" = "‣ Regenerate episodes.json manually" ]; then
  # Generate episodes-list.md
  # Remove previous markdown generated before regenerating
  rm "$project_root"/"$folder_webcomics"/.episodes-list.md
  webcomics_dir="$project_root"/"$folder_webcomics"
  # Loop through directories matching the pattern
  for episode_dir in "$webcomics_dir"/ep*_*/; do
      # Check if info.json exists in the directory
      if [[ -f "${episode_dir}info.json" ]]; then
          # Use jq to check if "public" is set to 1
          if jq -e '.public == 1' "${episode_dir}info.json" > /dev/null 2>&1; then
              # Process the directory as it meets the criteria
              echo "[episodes-list.md] Processing directory: $episode_dir"
              episode_dir=$(basename "$episode_dir")
              echo "$episode_dir" >> "$project_root"/"$folder_webcomics"/.episodes-list.md
          fi
      fi
  done

  # Generate episodes-v1.json and episodes.json
  rm "$project_root"/"$folder_webcomics"/episodes.json
  "$project_root/$folder_tools/utils/generate_json_episodes_lists.py" "$project_root/$folder_webcomics" "$project_root/$folder_webcomics/episodes-v1.json" "$project_root/$folder_webcomics/episodes.json"

  exit
  
elif [ "$menuchoicecleaned" = "‣ Uploader" ]; then
  function_run_in_terminal "$project_root/$folder_tools/uploader.sh"
 
elif [ "$menuchoicecleaned" = "‣ Website" ]; then
  function_run_in_terminal "$project_root/$folder_tools/update-website.sh"
  
elif [ "$menuchoicecleaned" = "‣ Wiki & Doc" ]; then
  function_run_in_terminal "$project_root/$folder_tools/update-wiki-and-doc.sh"

elif [ "$menuchoicecleaned" = "‣ Community update" ]; then
  function_run_in_terminal "$project_root/$folder_tools/update-community.sh"

elif [ "$menuchoicecleaned" = "‣ Validate transcript" ]; then
  function_run_in_terminal "$project_root/$folder_tools/validate-transcript-wrapper.sh"

elif [ "$menuchoicecleaned" = "‣ Optimize the PNG of the new- episode" ]; then
  function_run_in_terminal "$project_root/$folder_tools/utils/optimize-png-lang-git.sh"

elif [ "$menuchoicecleaned" = "‣ Convert a Transcript to a Pad" ]; then
  function_run_in_terminal "$project_root/$folder_tools/utils/convert_transcript-to-framapad.sh"

elif [ "$menuchoicecleaned" = "‣ Render all" ]; then
  function_run_in_terminal "$project_root/$folder_tools/update-all.sh --prompt"

elif [ "$menuchoicecleaned" = "+ Add a new episode" ]; then
  function_run_in_terminal "$project_root/$folder_tools/add-a-new-episode.sh"

else 
  # Cheap method to open the sub menu for episodes options 
  function_launch_render
fi

# Footer library
function_load_source_lib "$script_absolute_path"/lib/footer.sh
