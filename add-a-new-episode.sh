#!/bin/bash

#: Title       : Add a new episode
#: Author      : David REVOY < info@davidrevoy.com >
#: License     : GPL

# Information
export script_title="Add a new episode"
export script_version="2.0a"
export script_filename="add-a-new-episode.sh"

# Setup the default lang
export lang="fr"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config

# Script start
# ============

# Display a menu with basic questions:
function_menu () {
    cd "$project_root"/"$folder_webcomics"
    echo -n "${Green} Enter episode number (XX, 01 to 99) to generate ${Off}"
    read episodenumber
    echo "${Green} Pepper&Carrot episode $episodenumber ${Off}"
    echo -n "${Green} ... with how many page (1~99) ? ${Off}"
    read pagemax
    echo "${Green} $pagemax pages, OK. Working : ${Off}"
}


function_create_new_episode () {

    export template_folder="$project_root"/"$folder_tools"/lib/new-episode_template/

    cd "$project_root"/"$folder_webcomics"

    # Exit if folder already exist.
    if [ -d "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here ]; then
        echo "${Red}==> [error] New directory already exist. Rename it and restart later.${Off}"
        exit
    fi

    # Directories and subdirectories
    echo "${Blue}==> ${Yellow} Generating directories: ${Off} /new-ep"$episodenumber"_Title-Here and lang/, lang/${lang}/ and xyz/"
    mkdir -p "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/${lang}
    mkdir -p "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/xyz/01_alpha1FR_description

    cd "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here

    # Header episode Title
    echo "${Blue}==> ${Yellow} Generating Header (episode Title): ${Off}" E"$episodenumber"P00
    cp "$template_folder"/header.kra "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/E"$episodenumber"P00.kra
    cp "$template_folder"/header.svg "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/${lang}/E"$episodenumber"P00.svg
    sed -i 's/!XX/'$episodenumber'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/${lang}/E"$episodenumber"P00.svg
    svgfiles=E"$episodenumber"P00.svg
    sed -i 's/EYYP00/'E"$episodenumber"P00'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/${lang}/"$svgfiles"

    # Cover: copy template
    echo "${Blue}==> ${Yellow} Copying Cover template: ${Off} E$episodenumber "
    cp "$template_folder"/cover.kra "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/E"$episodenumber".kra

    # Pages
    count=1
    while [ $count -le $pagemax ]; do
      echo "${Blue}==> ${Yellow} Generating Pages: ${Off}" E"$episodenumber"P"$(printf %02d $count)"
      cp "$template_folder"/page.kra "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/E"$episodenumber"P"$(printf %02d $count)".kra
      cp "$template_folder"/page.svg "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/${lang}/E"$episodenumber"P"$(printf %02d $count)".svg
      svgfiles=E"$episodenumber"P"$(printf %02d $count)".svg
      sed -i 's/EYYPXX/'E"$episodenumber"P"$(printf %02d $count)"'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/${lang}/"$svgfiles"
      sed -i 's/!XX/'"$(printf %02d $count)"'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/${lang}/"$svgfiles"
      ((count++))
      echo "${Blue} ------------------------------- ${Off}"
      echo ""
    done

    # README.md
    echo "${Blue}==> ${Yellow} Generating: ${Off} README.md"
    cp "$template_folder"/README.md "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/README.md
    sed -i 's/YY/'$episodenumber'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/README.md

    # Beeref references (empty)
    echo "${Blue}==> ${Yellow} Generating: ${Off} ep"$episodenumber"_references.bee"
    cp "$template_folder"/ref.bee "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/ep"$episodenumber"_references.bee

    # Get Software Versions
    echo "${Blue}==> ${Yellow} Updating Software versions on templates and credits${Off}"
    version_inkscape="$(inkscape --version | sed -rn "s/^inkscape ([0-9.]*) .*$/\1/ip" || echo "???")"
    version_krita="$(/home/deevad/sources/krita/krita-stable --version | sed -rn "s/^krita ([a-z0-9.\-]*)$/\1/ip" || echo "???")"
    source /etc/os-release
    version_system_name=$NAME
    version_system_version=$VERSION

    # Apply Software Versions to info.json (at root of episode)
    sed "
        s/%ID%/$(printf %d $episodenumber)/g
        s/%SERIAL%/$(( $episodenumber - 1 ))/g
        s/%VERSION_KRITA%/$version_krita/g
        s/%VERSION_INKSCAPE%/$version_inkscape/g
        s/%VERSION_SYSTEM_NAME%/$version_system_name/g
        s/%VERSION_SYSTEM_VERSION%/$version_system_version/g
    " "$template_folder"/info.json > "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/info.json

    # Credits
    echo "${Blue}==> ${Yellow} Generating Credits: ${Off}" E"$episodenumber"P"$(printf %02d $count)".svg
    cp "$template_folder"/credit.kra "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/E"$episodenumber"P"$(printf %02d $count)".kra
    # Apply Software Versions, rename xlink, while copying credits's svg
    sed "
        s/EYYPXX/E"$episodenumber"P"$(printf %02d $count)"/g
        s/%VERSION_KRITA%/$version_krita/g
        s/%VERSION_INKSCAPE%/$version_inkscape/g
        s/%VERSION_SYSTEM_NAME%/$version_system_name/g
        s/%VERSION_SYSTEM_VERSION%/$version_system_version/g
    " "$template_folder"/credit.svg > "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/${lang}/E"$episodenumber"P"$(printf %02d $count)".svg

    # Copy a JSON template, to credit author of the lang
    cp "$template_folder"/infoB.json "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/${lang}/info.json

    # Create a secret tokken: make the episode secret (don't publish the alpha on XYZ while this tokken is here)
    touch "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/secret

    # Launch renderfarm to create all other temp directories and rendering.
    echo ""
    echo "${Blue}==> ${Yellow} Start initial render:"
    "$script_absolute_path"/renderfarm.sh "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/

    # Clean the backup content and WIP.
    echo "${Blue}==> ${Yellow} Cleaning initial folder:"
    cd "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/"$folder_backup"
    rm -rf *.kra
    cd "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/"$folder_wip"
    rm -rf *.jpg
    cd ..

    ls -a | tee -a "$log_file"

    echo "${Green} Done. Episode created in the 'New' folder. Rename it when you have a title. ${Off}"

}

# Script
function_decorative_header
function_menu
function_create_new_episode

# Footer
function_load_source_lib "$script_absolute_path"/lib/footer.sh

if [ "$1" = "--prompt" ]; then
# Task is executed inside a terminal window (pop-up)
# This line prevent terminal windows to be auto-closed
# and necessary to read log later, or to reload the script
 echo "${Purple}  Press [Enter] to exit or [r](then Enter) to reload $script_filename${Off}"
 read -p "?" ANSWER
 if [ "$ANSWER" = "r" ]; then
   "$project_root"/"$folder_tools"/"$script_filename" --prompt
 fi
fi
